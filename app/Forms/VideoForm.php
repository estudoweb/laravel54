<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class VideoForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('title', 'text', [
                'label' => 'Title',
                'rules' => 'required|max:255'
            ])
            ->add('description', 'textarea', [
                'label' => 'Description',
                'rules' => "required"
            ]);
    }
}
