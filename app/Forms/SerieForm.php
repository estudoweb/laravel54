<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class SerieForm extends Form
{
    public function buildForm()
    {
        $id = $this->getData('id');

        $this
            ->add('title', 'text', [
                'label' => 'Title',
                'rules' => 'required|max:255'
            ])
            ->add('description', 'text', [
                'label' => 'Description',
                'rules' => "required|max:255"
            ]);
    }
}
