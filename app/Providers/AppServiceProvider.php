<?php

namespace App\Providers;

use Illuminate\Auth\AuthenticationException;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Dingo\Api\Exception\Handler;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $handler = app(Handler::class);

        $handler->register(function(AuthenticationException $exception){
           return response()->json(['error' => 'Unhauthenticated'], 401);
        });
    }
}
