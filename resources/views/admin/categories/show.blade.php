@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row">
        <h3>Visualizar Usuário</h3>

        <div class="row">
            {!! Button::primary('Editar')->asLinkTo(route('admin.categories.edit', ['category' => $category->id])) !!}
            {!! Button::danger('Excluir')->asLinkTo(route('admin.categories.destroy', ['category' => $category->id]))
                ->addAttributes(['onclick' => "event.preventDefault(); document.getElementById(\"form-delete\").submit();"])
            !!}
            <?php $formDelete = FormBuilder::plain([
                                                    'id' => 'form-delete',
                                                    'route' => ['admin.categories.destroy', 'category' => $category->id],
                                                    'method' => 'DELETE',
                                                    'style' => 'display:none'
                                                    ]
                                                );
                    ?>
            {!! form($formDelete) !!}

            <br />
            <br />
            <table class="table table-bordered">
                <tbody>
                <tr>
                    <td scope="row">#</td>
                    <td>{{$category->id}}</td>
                </tr>
                <tr>
                    <td scope="row">Nome</td>
                    <td>{{$category->name}}</td>
                </tr>

                </tbody>
            </table>
        </div>
        
    </div>
</div>
@endsection
