@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row">
        <h3>Lista de Categorias</h3>
        {!! Button::primary('Nova categoria')->asLinkTo(route('admin.categories.create')) !!}
    </div>
    <div class="row">
       {!! Table::withContents($categories->items())->striped()
            ->callback('Ações', function($field, $categorie) {
                $linkEdit = route('admin.categories.edit', ['category' => $categorie->id]);
                $linkShow = route('admin.categories.show', ['category' => $categorie->id]);
                return Button::link(Icon::create('pencil'))->asLinkTo($linkEdit) . "|" .
                        Button::link(Icon::create('remove'))->asLinkTo($linkShow);
            })
        !!}

        {!! $categories->links() !!}
    </div>
</div>
@endsection
