@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row">
        <h3>Mudar Senha</h3>
        {!! form($form->add('salve','submit', [
         'attr' => ['class' => 'btn btn-primary btn-block'],
            'label' => 'Salvar'
         ])) !!}
    </div>
</div>
@endsection
