@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row">
        <h3>Visualizar Usuário</h3>

        <div class="row">
            {!! Button::primary('Editar')->asLinkTo(route('admin.users.edit', ['user' => $user->id])) !!}
            {!! Button::danger('Excluir')->asLinkTo(route('admin.users.destroy', ['user' => $user->id]))
                ->addAttributes(['onclick' => "event.preventDefault(); document.getElementById(\"form-delete\").submit();"])
            !!}
            <?php $formDelete = FormBuilder::plain([
                                                    'id' => 'form-delete',
                                                    'route' => ['admin.users.destroy', 'user' => $user->id],
                                                    'method' => 'DELETE',
                                                    'style' => 'display:none'
                                                    ]
                                                );
                    ?>
            {!! form($formDelete) !!}

            <br />
            <br />
            <table class="table table-bordered">
                <tbody>
                <tr>
                    <td scope="row">#</td>
                    <td>{{$user->id}}</td>
                </tr>
                <tr>
                    <td scope="row">Nome</td>
                    <td>{{$user->name}}</td>
                </tr>
                <tr>
                    <td scope="row">E-mail</td>
                    <td>{{$user->email}}</td>
                </tr>

                </tbody>
            </table>
        </div>
        
    </div>
</div>
@endsection
