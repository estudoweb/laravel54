@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row">
        <h3>Visualizar Usuário</h3>

        <div class="row">
            {!! Button::primary('Editar')->asLinkTo(route('admin.series.edit', ['serie' => $serie->id])) !!}
            {!! Button::danger('Excluir')->asLinkTo(route('admin.series.destroy', ['serie' => $serie->id]))
                ->addAttributes(['onclick' => "event.preventDefault(); document.getElementById(\"form-delete\").submit();"])
            !!}
            <?php $formDelete = FormBuilder::plain([
                                                    'id' => 'form-delete',
                                                    'route' => ['admin.series.destroy', 'serie' => $serie->id],
                                                    'method' => 'DELETE',
                                                    'style' => 'display:none'
                                                    ]
                                                );
                    ?>
            {!! form($formDelete) !!}

            <br />
            <br />
            <table class="table table-bordered">
                <tbody>
                <tr>
                    <td scope="row">#</td>
                    <td>{{$serie->id}}</td>
                </tr>
                <tr>
                    <td scope="row">Title</td>
                    <td>{{$serie->title}}</td>
                </tr>

                </tbody>
            </table>
        </div>
        
    </div>
</div>
@endsection
