@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row">
        <h3>Visualizar Usuário</h3>

        <div class="row">
            {!! Button::primary('Editar')->asLinkTo(route('admin.videos.edit', ['video' => $video->id])) !!}
            {!! Button::danger('Excluir')->asLinkTo(route('admin.videos.destroy', ['video' => $video->id]))
                ->addAttributes(['onclick' => "event.preventDefault(); document.getElementById(\"form-delete\").submit();"])
            !!}
            <?php $formDelete = FormBuilder::plain([
                                                    'id' => 'form-delete',
                                                    'route' => ['admin.videos.destroy', 'video' => $video->id],
                                                    'method' => 'DELETE',
                                                    'style' => 'display:none'
                                                    ]
                                                );
                    ?>
            {!! form($formDelete) !!}

            <br />
            <br />
            <table class="table table-bordered">
                <tbody>
                <tr>
                    <td scope="row">#</td>
                    <td>{{$video->id}}</td>
                </tr>
                <tr>
                    <td scope="row">Title</td>
                    <td>{{$video->title}}</td>
                </tr>

                <tr>
                    <td scope="row">Description</td>
                    <td>{{$video->description}}</td>
                </tr>

                </tbody>
            </table>
        </div>
        
    </div>
</div>
@endsection
