@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row">
        @component('admin.videos.tabs-component')
            <div class="col-md-12">
                <h4>Novo Vídeo</h4>
                {!! form($form->add('salve','submit', [
                 'attr' => ['class' => 'btn btn-primary btn-block'],
                    'label' => 'Salvar'
                 ])) !!}
            </div>
        @endcomponent
    </div>
</div>
@endsection
