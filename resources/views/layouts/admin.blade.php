<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    @stack('styles')
</head>
<body>
    <div id="app">
        <?php

                $navbar = Navbar::withBrand(config('app.name'), url('/admin/dashboard'))->inverse();

                if(Auth::check()) {
                    $arrayLinks = [
                        ['link' => route('admin.users.index'), 'title' => 'Usuário'],
                        ['link' => route('admin.categories.index'), 'title' => 'Categoria'],
                        ['link' => route('admin.series.index'), 'title' => 'Series'],
                        ['link' => route('admin.videos.index'), 'title' => 'Vídeos'],
                    ];
                    $menus = Navigation::links($arrayLinks);
                    $menuRight = Navigation::links([[
                        Auth::user()->name,
                        [
                            [
                                'link' => route('admin.logout'),
                                'title' => 'Logout',
                                'linkAttributes' => [
                                    'onclick' => "event.preventDefault(); document.getElementById(\"form-logout\").submit();"
                                ]
                            ],
                            [
                                'link' => route('admin.user_settings.edit'),
                                'title' => 'Configurações'
                            ]
                        ]
                    ]])->right();
                    $navbar->withContent($menus)
                        ->withContent($menuRight);
                }

        ?>
        {!! $navbar !!}
            <?php $formLogOut = FormBuilder::plain([
                        'id' => 'form-logout',
                        'route' => ['admin.logout'],
                        'method' => 'POST',
                        'style' => 'display:none'
                    ]
                );
            ?>
        {!! form($formLogOut) !!}

        @if(Session::has('message'))
            <div class="container">
                {!! Alert::success(Session::get('message'))->close() !!}
            </div>
        @endif

        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>
